<?= $this->extend('layouts/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h2 class="mt-2">Edit produk</h2>
            <form action="/product/update/<?= $product['id']; ?>" method="post">
                <?= csrf_field() ?>
                <div class="form-group">
                    <label for="nama">Nama Produk</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="<?= $product['nama']; ?>" placeholder="Nama produk">
                </div>
                <div class="form-group">
                    <label for="harga">Harga</label>
                    <input type="text" class="form-control" id="harga" name="harga" value="<?= $product['harga']; ?>" placeholder="Harga produk">
                </div>
                <button type="submit" class="btn btn-primary">Ubah</button>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<?= $this->endSection(); ?>