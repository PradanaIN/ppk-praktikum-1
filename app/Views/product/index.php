<?= $this->extend('layouts/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2 class="mt-2">Daftar Produk</h2>
                        <a href="/product/add" class="btn btn-primary mb-3">Tambah Produk</a>

                        <?php if (session()->getFlashdata('pesan')) : ?>
                            <div class="alert alert-success" role="alert">
                            </div>
                        <?php endif; ?>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Product Name</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php foreach ($product as $p) : ?>
                                    <tr>
                                        <th scope="row"> <?= $i; ?></th>
                                        <td><?= $p['nama']; ?></td>
                                        <td><?= $p['harga']; ?></td>
                                        <td>
                                            <a href="/product/edit/<?= $p['id'] ?>" class="btn btn-success">Update</a>
                                            <form action="/product/<?= $p['id']; ?>" method="post" class="d-inline">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="submit" class="btn btn-danger" onclick="return confirm('Hapus data?');">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>