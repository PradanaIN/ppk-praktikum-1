<?php

namespace App\Controllers;

class Pages extends BaseController
{
    public function index()
    {
        // data for web title
        $data = [
            'title' => 'Home | Belajar CodeIgniter'
        ];

        echo view('pages/home', $data);
    }

    public function about()
    {
        $data = [
            'title' => 'About Me!'
        ];

        // bisa return karena cuma 1
        return view('pages/about', $data);
    }

    public function contact()
    {
        $data = [
            'title' => 'Get in Touch!',

            'alamat' => [
                'tipe' => 'Kantor',
                'alamat' => 'Otista 64C',
                'kota' => 'Jakarta Timur'
            ],
            [
                'tipe' => 'Kos',
                'alamat' => 'Kebon Sayur',
                'kota' => 'Jakarta Timur'
            ]

        ];

        // bisa return karena cuma 1
        return view('pages/contact', $data);
    }
}
