<?php

namespace App\Controllers;

use App\Models\ProductModel;

class Product extends BaseController
{
    protected $productModel;

    public function __construct()
    {
        $this->productModel = new ProductModel();
    }

    public function index()
    {
        $product = $this->productModel->findAll();

        $data = [
            'title' => 'Daftar Produk',
            'product' => $product
        ];


        return view('product/index', $data);
    }

    public function add()
    {
        $data = [
            'title' => 'Form tambah produk'
        ];

        return view('product/add', $data);
    }

    public function save()
    {
        $this->productModel->save([
            'nama' => $this->request->getPost('nama'),
            'harga' => $this->request->getPost('harga')
        ]);

        session()->setFlashdata('pesan', 'Produk berhasil ditambahkan!');

        return redirect()->to('/product/index');
    }

    public function delete($id)
    {
        $this->productModel->delete($id);
        session()->setFlashdata('pesan', 'Produk berhasil dihapus!');
        return redirect()->to('/product/index');
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Form edit produk',
            'product' => $this->productModel->find($id)
        ];

        return view('product/edit', $data);
    }

    public function update($id)
    {
        $this->productModel->save([
            'id' => $id,
            'nama' => $this->request->getVar('nama'),
            'harga' => $this->request->getVar('harga')
        ]);

        session()->setFlashdata('pesan', 'Produk berhasil diupdate!');

        return redirect()->to('/product/index');
    }
}
